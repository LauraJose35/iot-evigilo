﻿using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace IoteVigilo.Tools
{
    public class MensajeRecibido
    {
        public string Topico { get; set; }
        public string Mensaje { get; set; }
    }
    public static class MqttService
    {
        static MQTTClient mqtt;
        public static event EventHandler<MensajeRecibido> MensajeRecibido;
        public static event EventHandler Conectado;
        private static string NombreCliente;

        public static void Conectar(string nombreCliente, string server, int puerto = 1883)
        {
            mqtt = new MQTTClient(server, puerto);
            NombreCliente = nombreCliente;
            Connect();
            mqtt.Connected += Mqtt_Connected;
            mqtt.MessageReceived += Mqtt_MessageReceived;
            mqtt.Disconnected += Mqtt_Disconnected;
        }

        private static void Mqtt_Disconnected(object sender, EventArgs e)
        {
            Task.Delay(2000);
            Connect();
        }

        private static void Mqtt_Connected(object sender, EventArgs e)
        {
            Debug.WriteLine($"Conectado a MQTT");
            Conectado(null, null);
        }

        private static void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            Debug.WriteLine("<- "+System.Text.Encoding.UTF8.GetString(payload));
            MensajeRecibido(null, new Tools.MensajeRecibido() 
            { 
                Mensaje=System.Text.Encoding.UTF8.GetString(payload),
                Topico=topic
            });
        }

        private static void Connect() => mqtt.Connect(NombreCliente);

        public static void Suscribir(string topic) => mqtt.Subscriptions.Add(new Subscription(topic));

        public static void Publicar (string topic, string mensaje)
        {
            Debug.WriteLine($"-> [{topic} {mensaje}]");
            mqtt.Publish(topic, mensaje, QoS.FireAndForget, false);
        }
    }
}
