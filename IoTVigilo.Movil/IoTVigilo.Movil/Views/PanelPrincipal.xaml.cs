﻿using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IoTVigilo.Movil.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PanelPrincipal : ContentPage
    {
        PanelUsuarioModel DatosPanelUsuario;
        string IdUsuario;
        public PanelPrincipal(string IdUsuario)
        {
            InitializeComponent();
            this.IdUsuario = IdUsuario;
            DatosPanelUsuario = new PanelUsuarioModel();
            DatosPanelUsuario.Usuario = FabricManager.Usuario.BuscarPorId(IdUsuario);
            DatosPanelUsuario.Proyectos = FabricManager.Proyecto.ListaDeProyectosPorId(IdUsuario).ToList();
            BindingContext = DatosPanelUsuario;
        }

        //private void listProyectos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        //{
        //    if (DatosPanelUsuario.ProyectoSeleccionado != null)
        //    {
        //        Navigation.PushAsync(new Sensores(DatosPanelUsuario.ProyectoSeleccionado.Id));
        //    }
        //}

        private void btnAgregarProyecto_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgregarProyecto(DatosPanelUsuario.Usuario.Id));
        }

        private void btnVerProyecto_Clicked(object sender, EventArgs e)
        {
            if (DatosPanelUsuario.ProyectoSeleccionado != null)
            {
                Navigation.PushAsync(new Sensores(DatosPanelUsuario.ProyectoSeleccionado.Id));
            }
            else
            {
                DisplayAlert("Error", "Seleccione un proyecto para ver sus detalles", "Ok");
            }
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            if (DatosPanelUsuario.ProyectoSeleccionado != null)
            {
                IProyecto repositorioProyecto = FabricManager.Proyecto;
                if (repositorioProyecto.Eliminar(DatosPanelUsuario.ProyectoSeleccionado.Id))
                {
                    DisplayAlert("Proyecto", $"El proyecto {DatosPanelUsuario.ProyectoSeleccionado.Nombre} se ha eliminado correctamente", "Ok");
                    DatosPanelUsuario.Proyectos = FabricManager.Proyecto.ListaDeProyectosPorId(IdUsuario).ToList();
                    listProyectos.ItemsSource = null;
                    listProyectos.ItemsSource = DatosPanelUsuario.Proyectos ;
                }
            }
            else
            {
                DisplayAlert("Error", "Seleccione un proyecto para Eliminar", "Ok");
            }
        }
    }
}