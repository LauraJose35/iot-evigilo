﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IoTVigilo.Movil.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarSensor : ContentPage
    {
        ISensor repositorioSensor;
        Proyecto Proyecto;
        Sensor models;
        string IdProyecto;
        public AgregarSensor(string IdProyecto)
        {
            InitializeComponent();
            repositorioSensor = FabricManager.SensorManager;
            models = BindingContext as Sensor;
            this.IdProyecto = IdProyecto;
            Proyecto = FabricManager.Proyecto.BuscarPorId(IdProyecto);
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            models.Id_Proyecto = Proyecto.Id;
            models.Id_Usuario = Proyecto.Id_Usuario;
            if (repositorioSensor.Agregar(models)) 
            {
                DisplayAlert("Sensor", "Guardado con exito", "Ok");
                Navigation.PushAsync(new Sensores(IdProyecto));
            }
            else
            {
                DisplayAlert("Sensor",repositorioSensor.Error, "Ok");
            }
        }
    }
}