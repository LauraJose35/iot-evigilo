﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IoTVigilo.Movil.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Sensores : ContentPage
    {
        Proyecto Proyecto;
        public List<Sensor> SensorActuadores { get; set; }
        string IdProyecto;
        public Sensores(string IdProyecto)
        {
            InitializeComponent();
            Proyecto = FabricManager.Proyecto.BuscarPorId(IdProyecto);
            SensorActuadores = FabricManager.SensorManager.ListaDeSensoresPorId(Proyecto.Id_Usuario, Proyecto.Id).ToList();
            this.IdProyecto = IdProyecto;
            listSensores.ItemsSource = null;
            listSensores.ItemsSource = SensorActuadores;
        }      

        private void btnAgregarSensor_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgregarSensor(IdProyecto));
        }      

        private void btnVeSensor_Clicked(object sender, EventArgs e)
        {
            Sensor sensor = listSensores.SelectedItem as Sensor;
            if (sensor != null)
            {
                Navigation.PushAsync(new DetalleSensor(sensor));
            }
            else
            {
                DisplayAlert("Error", "Seleccione un sensor para ver sus detalles", "Ok");
            }
        }

        private void btnEliminar_Clicked(object sender, EventArgs e)
        {
            Sensor sensor = listSensores.SelectedItem as Sensor;
            if (sensor != null)
            {
                ISensor repositorioSensor = FabricManager.SensorManager;
                if (repositorioSensor.Eliminar(sensor.Id))
                {
                    DisplayAlert("Sensor", $"El sensor {sensor.Nombre} se ha eliminado correctamente", "Ok");
                    SensorActuadores = FabricManager.SensorManager.ListaDeSensoresPorId(Proyecto.Id_Usuario, Proyecto.Id).ToList();
                    listSensores.ItemsSource = null;
                    listSensores.ItemsSource = SensorActuadores;
                }
            }
            else
            {
                DisplayAlert("Error", "Seleccione un sensor para Eliminar", "Ok");
            }
        }
    }
}