﻿
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using IoTVigilo.Movil.Views;
using Microsoft.CognitiveServices.Speech;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace IoTVigilo.Movil
{
    public partial class MainPage : ContentPage
    {
        Login model;
        IUsuario repositoryUsuario;
        public MainPage()
        {
            InitializeComponent();
            model = BindingContext as Login;
            repositoryUsuario = FabricManager.Usuario;

        }

        private void btnIngresar_Clicked(object sender, EventArgs e)
        {
            try
            {
                Usuario u = repositoryUsuario.Login(model.Correo, model.Password);
                if (u != null)
                {
                   LeerBienvenida($"Hola {u.Nombre}");
                   DisplayAlert("","Bienvenid@ "+ u.Nombre, "Ok");
                    Navigation.PushAsync(new PanelPrincipal(u.Id));
                    //Navigation.PushAsync(new Ejemplo());
                }
                else
                {
                   DisplayAlert("Error", "El usuario o contraseña es invalido","Ok");
                }                
            }
            catch (Exception ex)
            {
                DisplayAlert("Error", ex.Message, "Ok");
            }
        }

        private static async void LeerBienvenida(string text)
        {
            string resultado="";
            try
            {
                var config = SpeechConfig.FromSubscription("4eb5040af25b477e85ea44acb5ce3e73", "eastus");
                var language = "es-ES";
                config.SpeechSynthesisLanguage = language;
                using (var synthesizer= new SpeechSynthesizer(config))
                {
                    using (var result=await synthesizer.SpeakTextAsync(text))
                    {
                        if (result.Reason == ResultReason.SynthesizingAudioCompleted)
                        {
                            resultado += "LecturaCorrecta";
                        }
                        else if (result.Reason == ResultReason.Canceled)
                        {                                  
                            var cancellation = SpeechSynthesisCancellationDetails.FromResult(result);
                            resultado += $"Cancelado: Razones= {cancellation.Reason}";
                            if (cancellation.Reason == CancellationReason.Error)
                            {
                                resultado += $"Cancelado: ErrorCode: {cancellation.ErrorCode}";
                                resultado += $"Cancelado: ErrorDetails: {cancellation.ErrorDetails}"; 
                                resultado += $"Cancelado: Necesitas actualizar la información de tu suscripción";
                            }
                            
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Registrarse_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Registro());
        }
    }
}
