﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using Plugin.Media;
using Plugin.Media.Abstractions;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace IoTVigilo.Movil.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registro : ContentPage
    {
        Usuario model;
        IUsuario repositioryUsuario;
        MediaFile foto;
        public Registro()
        {
            InitializeComponent();
            model = BindingContext as Usuario;
            repositioryUsuario = FabricManager.Usuario;
        }

        private void btnRegistrar_Clicked(object sender, EventArgs e)
        {
            if (repositioryUsuario.Agregar(model))
            {
                DisplayAlert("Registro", "Exitoso", "Ok");
                Navigation.PushAsync(new MainPage());
            }
            else
            {
                DisplayAlert("Registro", repositioryUsuario.Error, "Ok");
            }
        }

        private async void btnCargaFoto_Clicked(object sender, EventArgs e)
        {
            foto =await TomarFoto();
            if (foto != null) {
                Imagen.Source = ImageSource.FromStream(foto.GetStream);
                model.Foto = ImageSource.FromStream(foto.GetStream);
            }
        }

        private Task<MediaFile> TomarFoto()
        {
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                DisplayAlert("Error", "Foto no soportada", "OK");
                return null;
            }
            var file = CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
            });

            return file==null?null:file;
            //Imagen.Source = ImageSource.FromStream(() =>
            //{
            //    var stream = file.GetStream();
            //    file.Dispose();
            //    return stream;
            //});
        }
    }
}