﻿
using IoTVigilo.Movil.Models;
using IoTVigilo.Movil.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IoTVigilo.Movil.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetalleSensorAnomalia : ContentPage
	{
        public List<LecturasModel> lecturas { get; set; }
		LecturasAnomalia lecturasAnomalia;

		public DetalleSensorAnomalia (List<LecturasModel> lecturas)
		{
			InitializeComponent ();
			this.lecturas = lecturas;
			lecturasAnomalia = new LecturasAnomalia(lecturas);
			chartgrafica.Chart = lecturasAnomalia.Chart;
		}

        private async void btnAnalizador_Clicked(object sender, EventArgs e)
        {
            lecturasAnomalia.DataPointEx = new System.Collections.ObjectModel.ObservableCollection<DataPointEx>();
            lecturasAnomalia.Sensitivity = lecturasAnomalia.Sensitivity;
            lecturasAnomalia.DataResult = await AnomalyDetectorService.AnalyzeData(lecturasAnomalia.DataRequest);
            if (lecturasAnomalia.DataResult != null)
            {
                for (int i = 0; i < lecturasAnomalia.DataResult.IsAnomaly.Length; i++)
                {
                    if (lecturasAnomalia.DataResult.IsAnomaly[i])
                    {
                        var point = lecturasAnomalia.DataRequest.Series[i];
                        lecturasAnomalia.DataPointEx.Add(new DataPointEx()
                        {
                            Value = point.Valor,
                            Timestamp = point.Fecha,
                            IsAnomaly = true
                        });
                    }
                }
                lecturasAnomalia.CrearChart(true);
                chartgrafica.Chart = lecturasAnomalia.Chart;
            }

        }
			

	}
}