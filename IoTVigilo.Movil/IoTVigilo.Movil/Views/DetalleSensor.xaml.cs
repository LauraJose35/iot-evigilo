﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using IoTVigilo.Movil.Models;
using IoTVigilo.Movil.Servicios;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using LecturasModel = IoTVigilo.Movil.Models.LecturasModel;

namespace IoTVigilo.Movil.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalleSensor : ContentPage
    {
        public Sensor sensor { get; set; }
        public List<Lectura> Lecturas { get; set; }
        public List<LecturasModel> LecturasModel { get; set; }

        LecturasBalanceModel lecturasBalance;
        public DetalleSensor(Sensor sensor)
        {
            InitializeComponent();
            this.sensor = sensor;
            Title = "Detalles del sensor " + sensor.Nombre;
            lecturasBalance = new LecturasBalanceModel(sensor.Id);
            Grafica.Model = null;
            Grafica.Model = lecturasBalance.GraficoModel;
            Grafica.BackgroundColor=Color.White;
            listLectura.ItemsSource = null;
            listLectura.ItemsSource = lecturasBalance.LecturaSensorSeleccionado;
            dtInicio.Date = DateTime.Now;
            dtFin.Date = DateTime.Now;
        }

        private void btnBuscar_Clicked(object sender, EventArgs e)
        {
            if (lecturasBalance.Inicio != null || lecturasBalance.Fin != null)
            {
                if (dtInicio.Date > dtFin.Date)
                {
                    DisplayAlert("Error", "La Fecha inicio debe ser menor a la fecha Fin", "Ok");
                    return;
                }
                lecturasBalance.Inicio = dtInicio.Date;
                lecturasBalance.Fin = dtFin.Date;
                lecturasBalance.ObtenerListaPorFecha(sensor.Id);
                Grafica.Model = null;
                Grafica.Model = lecturasBalance.GraficoModel;
                Grafica.BackgroundColor = Color.White;
                listLectura.ItemsSource = null;
                listLectura.ItemsSource = lecturasBalance.LecturaSensorSeleccionado;

            }
            else
            {
                DisplayAlert("Detalle sensor", "Seleccione las fechas de inicio y fin", "Ok");
            }
        }

        private async void btnDescargarArchivo_Clicked(object sender, EventArgs e)
        {
            ExcelService excelService;
             excelService = new ExcelService();
            var fileName = "Lecturas.xlsx";
            string filePath = excelService.GenerateExcel(fileName);

            var header = new List<string>() { "Fecha hora", "Valor", "Unidad de valor" };

            var data = new ExcelData();
            data.Headers = header;

            foreach (var publication in lecturasBalance.LecturaSensorSeleccionado)
            {
                var row = new List<string>()
                {
                    publication.FechaHora.ToString(),
                    publication.Valor.ToString(),
                    publication.UnidadValor.ToString(),
                };

                data.Values.Add(row);
            }

            excelService.InsertDataIntoSheet(filePath, "Lecturas", data);

            await Launcher.OpenAsync(new OpenFileRequest()
            {
                File = new ReadOnlyFile(filePath)
            });
        }

        private void btnBuscarAnomalia_Clicked(object sender, EventArgs e)
        {
            //Navigation.PushAsync(new DetalleSensorAnomalia(lecturasBalance.LecturaSensorSeleccionado));
        }
    }
}