﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace IoTVigilo.Movil.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgregarProyecto : ContentPage
    {
        Proyecto models;
        IProyecto repositoryProyecto;
        public AgregarProyecto(string IdUsuario)
        {
            InitializeComponent();
            models = BindingContext as Proyecto;
            models.Id_Usuario = IdUsuario;
            repositoryProyecto = FabricManager.Proyecto;
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            if (repositoryProyecto.Agregar(models))
            {
                DisplayAlert("Proyecto", "Exitoso", "Ok");
                Navigation.PushAsync(new PanelPrincipal(models.Id_Usuario));
            }
            else
            {
                DisplayAlert("Proyecto", repositoryProyecto.Error, "Ok");
            }
        }
    }
}