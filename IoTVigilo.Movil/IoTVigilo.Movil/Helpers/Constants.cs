﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTVigilo.Movil.Helpers
{
    public static class Constants
    {
        public static readonly string SubscriptionKey = "a458d14de5e548f3b21256be347a51dd";
        public static readonly string Endpoint = "https://adexchangeanomalydetector.cognitiveservices.azure.com/";
        public static readonly string DetectAnomaliesServiceURL = "anomalydetector/v1.0/timeseries/entire/detect";
    }
}
