﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTVigilo.Movil.Models
{
    public class LecturasModel
    {
        public DateTime FechaHora { get; set; }
        public float Valor { get; set; }
        public string UnidadValor { get; set; }

        public override string ToString()
        {
            return $"{FechaHora}, {Valor}, {UnidadValor}";
        }
    }
}
