﻿using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTVigilo.Movil.Models
{
    public class LecturasBalanceModel
    {
        Sensor SensorActuador;
        
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public List<LecturasModel> LecturaSensorSeleccionado { get; set; }
        public List<Lectura> ListaLectura { get; set; }
        public PlotModel GraficoModel { get; set; }
        public LecturasBalanceModel()
        {
                
        }
        public LecturasBalanceModel(string IdSensor)
        {
            SensorActuador = FabricManager.SensorManager.BuscarPorId(IdSensor);
            ListaLectura = FabricManager.Lectura.LecturasDelSensorId(IdSensor).OrderBy(p => p.FechaHora).ToList();
            ObtenerListaSensorSeleccionado();
            //Graficar();
            GraficarPorLinea();
        }

        private void Graficar()
        {
            GraficoModel = new PlotModel();
            var series = new TwoColorAreaSeries { Title = "Series" };
            Axis ejesX = new CategoryAxis()
            {
                Position = AxisPosition.Bottom,
                ItemsSource = LecturaSensorSeleccionado.Select(d => d.Valor)
            };
            Axis ejeY = new LinearAxis()
            {
                Position = AxisPosition.Left
            };

            LineSeries linea = new LineSeries();
            linea.ItemsSource = LecturaSensorSeleccionado.Select(d => d.FechaHora);
            GraficoModel.Axes.Add(ejesX);
            GraficoModel.Axes.Add(ejeY);
            GraficoModel.Series.Add(linea);
            linea.Title = "Valor";
            GraficoModel.Title = "Lecturas";


            //Axis ejesX = new CategoryAxis()
            //{
            //    Position = AxisPosition.Bottom,
            //    ItemsSource = LecturaSensorSeleccionado.Select(d => d.Valor)
            //};
            //Axis ejeY = new LinearAxis()
            //{
            //    Position = AxisPosition.Left
            //};
            //GraficoModel.Axes.Add(ejesX);
            //GraficoModel.Axes.Add(ejeY);
            //LineSeries linea = new LineSeries();
            //linea.ItemsSource = LecturaSensorSeleccionado.Select(d => d.FechaHora);
            //GraficoModel.Series.Add(linea);
            //linea.Title = "Valor";
            //GraficoModel.Title = "Lecturas";
        }
        private void GraficarPorLinea()
        {
            GraficoModel = new PlotModel();
            DateTimeAxis ejeTiempo = new DateTimeAxis();
            LineSeries valorSerie = new LineSeries();
            foreach (var item in LecturaSensorSeleccionado)
            {
                valorSerie.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.Valor));
            }
            valorSerie.Title = "Valor";
            GraficoModel.Axes.Add(ejeTiempo);
            GraficoModel.Series.Add(valorSerie);
        }
        private void ObtenerListaSensorSeleccionado()
        {
            LecturaSensorSeleccionado = new List<LecturasModel>();
            foreach (var item in ListaLectura)
            {
                LecturaSensorSeleccionado.Add(new LecturasModel()
                {
                    FechaHora = item.FechaHora,
                    Valor = item.Valor=="0"||item.Valor==""?0:float.Parse(item.Valor),
                    UnidadValor = SensorActuador.UnidadMedida,
                });
            }
        }

        public void ObtenerListaPorFecha(string IdSensor)
        {
            try
            {
                LecturaSensorSeleccionado = new List<LecturasModel>();
                foreach (var item in ListaLectura)
                {

                    if (item.FechaHora >= Inicio && item.FechaHora <= Fin)
                    {
                        LecturaSensorSeleccionado.Add(new LecturasModel()
                        {
                            FechaHora = item.FechaHora,
                            Valor = item.Valor == "0" || item.Valor == "" ? 0 : float.Parse(item.Valor),
                            UnidadValor = SensorActuador.UnidadMedida,
                        });
                    }
                }
                GraficarPorLinea();
            }
            catch (Exception)
            {

                throw;
            }          
        }


    }
}
