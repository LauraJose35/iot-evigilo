﻿using IoTVigilo.Movil.Servicios;
using Microcharts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace IoTVigilo.Movil.Models
{
    public class LecturasAnomalia
    {
        public bool IsBusy { get; set; }
        public int Sensitivity { get; set; }
        public DataRequest DataRequest { get; set; }
        public DataResult DataResult { get; set; }
        public ObservableCollection<DataPoint> DataPoints { get; set; }
        public ObservableCollection<DataPointEx> DataPointEx { get; set; }
        public Chart Chart { get; set; }
        public List<LecturasModel> Lecturas { get; set; }

        public Command AnalyzeDataCommand { get; set; }
        public LecturasAnomalia()
        {

        }
        public LecturasAnomalia(List<LecturasModel> lecturas)
        {
            IsBusy = true;
            Lecturas = lecturas;
            ObtenerDatos();
            CrearChart(anomalies:false);
          //  AnalyzeDataCommand = new Command(async () => await AnalyzeData());
            IsBusy = false;
        }

        public void CrearChart(bool anomalies)
        {
            Chart = null;
            Chart= new LineChart()
            { 
                LineMode= LineMode.Spline,
                LabelTextSize=0
            };
            Chart.Entries = DataRequest.Series.Select(
                (v, index) => new Microcharts.Entry(v.Valor)
                {
                    Label = v.Fecha.ToString("MM/dd_HH:mm"),
                    Color = anomalies
                                ? DataPointEx.Any(x => x.Timestamp.ToString("MM/dd_HH:mm") == v.Fecha.ToString("MM/dd_HH:mm"))
                                    ? SKColors.Red
                                    : SKColors.Green
                                : SKColors.Green,
                });
        }

        public void ObtenerDatos()
        {
            var series = FileService.Read(Lecturas);
            DataPoints = new ObservableCollection<DataPoint>(series);
            Sensitivity = 95;
            DataRequest = new DataRequest()
            {
                Granularity = "hourly",
                MaxAnomalyRatio = 0.25,
                Sensitivity = Sensitivity,
                Series = series
            };

            
        }

        public async Task AnalyzeData()
        {
            IsBusy = true;
            DataPointEx = new ObservableCollection<DataPointEx>();

            DataRequest.Sensitivity = Sensitivity;
            DataResult = await AnomalyDetectorService.AnalyzeData(DataRequest);

            if (DataResult != null)
            {
                for (int i = 0; i < DataResult.IsAnomaly.Length; i++)
                {
                    if (DataResult.IsAnomaly[i])
                    {
                        var point = DataRequest.Series[i];

                        DataPointEx.Add(new DataPointEx()
                        {
                            Value = point.Valor,
                            Timestamp = point.Fecha,
                            IsAnomaly = true
                        });
                    }
                }

                CrearChart(anomalies: true);
            }
            IsBusy = false;
        }

        //public async void GeneraDatosAnalisis()
        //{
        //    IsBusy = true;
        //    DataPointEx = new ObservableCollection<DataPointEx>();

        //    DataRequest.Sensitivity = Sensitivity;
        //    DataResult = await AnomalyDetectorService.AnalizaDatos(DataRequest);
        //}

       
    }
}
