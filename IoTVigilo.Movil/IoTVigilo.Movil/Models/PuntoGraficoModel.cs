﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoTVigilo.Movil.Models
{
    public class PuntoGraficoModel
    {
        public DateTime Fecha { get; set; }
        public float Valor { get; set; }
    }
}
