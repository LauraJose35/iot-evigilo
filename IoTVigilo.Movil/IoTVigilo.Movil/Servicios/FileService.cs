﻿using IotVigilio.COMMON.Entidades;
using IoTVigilo.Movil.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoTVigilo.Movil.Servicios
{
    public static class FileService
    {
        public static List<DataPoint> Read(List<LecturasModel> lecturas)
        {
            List<DataPoint> puntos = new List<DataPoint>();
            foreach (var item in lecturas)
            {
                puntos.Add(new DataPoint() {
                    Fecha=item.FechaHora.ToUniversalTime(),
                    Valor=item.Valor                
                });
            }
            return puntos;
        }
    }   
}
