﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using System.Linq.Expressions;
using MongoDB.Driver;
using System.Linq;
using FluentValidation.Results;
using LiteDB;

namespace IotVigilio.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _db;

        private AbstractValidator<T> _validator;
        bool resultado;
        ValidationResult validationResult;
        public GenericRepository(AbstractValidator<T> validator)
        {
            this._validator = validator;
           // _client = new MongoClient("mongodb://user:12345@iotevigilo-shard-00-00.3ytk0.mongodb.net:27017,iotevigilo-shard-00-01.3ytk0.mongodb.net:27017,iotevigilo-shard-00-02.3ytk0.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-gfm52c-shard-0&authSource=admin&retryWrites=true&w=majority");
            //_db = _client.GetDatabase("Plataforma_IoT");

             _client = new MongoClient("mongodb+srv://user:12345@iotevigilo.3ytk0.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
            _db = _client.GetDatabase("IoTeVigilo");


            //_client = new MongoClient("mongodb://user:12345@127.0.0.1:27017/iotdbname?authSource=admin");
            //_db = _client.GetDatabase("iotdbname");
        }

        private IMongoCollection<T> Collection() => _db.GetCollection<T>(typeof(T).Name);
        public string Error { get; private set; } = "";

        public IEnumerable<T> Read
        {
            get
            {
                try
                {
                    IEnumerable<T> datos = Collection().AsQueryable();
                    return datos == null ? throw new Exception("Error de conexión") : datos;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }

        public bool Create(T entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            entidad.FechaHora = DateTime.Now;
            validationResult = _validator.Validate(entidad);
            if (validationResult.IsValid)
            {
                try
                {
                    if (validationResult.IsValid)
                    {
                        Collection().InsertOne(entidad);
                        Error = "";
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            else
            {
                Error = "Datos invalidos: ";
                foreach (var error in validationResult.Errors)
                {
                    Error += $"\n>{error.ErrorMessage}";
                }
            }
            return resultado;
        }

        public bool Delete(string Id)
        {
            try
            {
                int r = (int)Collection().DeleteOne(p => p.Id == Id).DeletedCount;
                Error = r == 1 ? "" : "No se elimino el registro";
                return r == 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado) => Read.Where(predicado.Compile());

        public T SearchById(string Id) => Read.Where(e => e.Id == Id).SingleOrDefault();

        public bool Update(T entidad)
        {
            if (_validator.Validate(entidad).IsValid)
            {
                try
                {
                    entidad.FechaHora = DateTime.Now;
                    int r = (int)Collection().ReplaceOne(p => p.Id == entidad.Id, entidad).ModifiedCount;
                    resultado = r == 1;
                    Error = "";
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    resultado = false;
                }
            }
            return resultado;
        }
        //readonly string dbName = @"C:/8 semestre/bd/DBIoT.db";
        //public string Error { get; private set; }

        //readonly AbstractValidator<T> validator;
        //public IEnumerable<T> Read
        //{
        //    get
        //    {
        //        try
        //        {
        //            IEnumerable<T> entidades;
        //            using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
        //            {
        //                entidades = db.GetCollection<T>(typeof(T).Name).FindAll().ToList();
        //            }
        //            Error = entidades != null ? "" : "No se encontraron entidades";
        //            return entidades;
        //        }
        //        catch (Exception ex)
        //        {
        //            Error = ex.Message;
        //            return null;
        //        }
        //    }
        //}

        //public GenericRepository(AbstractValidator<T> validator)
        //{
        //    this.validator = validator;
        //}
        //public bool Create(T entidad)
        //{
        //    try
        //    {
        //        entidad.Id = Guid.NewGuid().ToString();


        //        entidad.FechaHora = DateTime.Now;

        //        var resultV = validator.Validate(entidad);
        //        if (resultV.IsValid)
        //        {
        //            using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
        //            {
        //                db.GetCollection<T>(typeof(T).Name).Insert(entidad);
        //            }
        //            Error = "";
        //            return true;
        //        }
        //        else
        //        {
        //            Error = "Errores de validación: ";
        //            foreach (var item in resultV.Errors)
        //            {
        //                Error += item.ErrorMessage + "; ";
        //            }
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Error = ex.Message;
        //        return false;
        //    }
        //}

        //public bool Delete(string Id)
        //{
        //    try
        //    {
        //        using (var db = new LiteDatabase(new ConnectionString { Filename = dbName }))
        //        {
        //            if (db.GetCollection<T>(typeof(T).Name).Delete(Id))
        //            {
        //                Error = "";
        //                return true;
        //            }
        //            else
        //            {
        //                Error = "No se pudo eliminar la entidad";
        //                return false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Error = ex.Message;
        //        return false;
        //    }
        //}

        //public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        //{
        //    try
        //    {
        //        IEnumerable<T> entidades;
        //        using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
        //        {
        //            entidades = db.GetCollection<T>(typeof(T).Name).Find(predicado).ToList();
        //        }
        //        Error = entidades != null ? "" : "No se encontraron entidades";
        //        return entidades;
        //    }
        //    catch (Exception ex)
        //    {
        //        Error = ex.Message;
        //        return null;
        //    }
        //}

        //public T SearchById(string Id)
        //{
        //    try
        //    {
        //        T entidad;
        //        using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
        //        {
        //            entidad = db.GetCollection<T>(typeof(T).Name).FindById(Id);
        //        }
        //        Error = entidad != null ? "" : "No se encontro la entidad";
        //        return entidad;
        //    }
        //    catch (Exception ex)
        //    {
        //        Error = ex.Message;
        //        return null;
        //    }
        //}

        //public bool Update(T entidad)
        //{
        //    try
        //    {
        //        var resultV = validator.Validate(entidad);
        //        if (resultV.IsValid)
        //        {
        //            bool r;
        //            using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
        //            {
        //                r = db.GetCollection<T>(typeof(T).Name).Delete(entidad.Id);
        //            }
        //            if (r)
        //            {
        //                Error = "";
        //                return true;
        //            }
        //            else
        //            {
        //                Error = "No se actualizo la entidad";
        //                return false;
        //            }
        //        }
        //        else
        //        {
        //            Error = "Errores de validación: ";
        //            foreach (var item in resultV.Errors)
        //            {
        //                Error += item.ErrorMessage + "; ";
        //            }
        //            return false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Error = ex.Message;
        //        return false;
        //    }
        //}
    }
}
