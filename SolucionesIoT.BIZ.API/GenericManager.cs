﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SolucionesIoT.BIZ.API
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal HttpClient client;
        internal string uriApi;
        internal string error;
        public GenericManager()
        {
            client = new HttpClient();
           // client.BaseAddress =new Uri("https://localhost:44328/");
           client.BaseAddress =new Uri("http://www.iot-evigilo.somee.com/");
            uriApi = "api/" + typeof(T).Name;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private async Task<IEnumerable<T>> ObtenerDatos() {
            HttpResponseMessage response = await client.GetAsync(uriApi).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var items = JsonConvert.DeserializeObject<IEnumerable<T>>(content);
                return items;
            }
            else {
                error = "No se pudieron obtener los datos";
                return null;
            }
        }

        private async Task<bool> Insertar(T entidad)
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(entidad), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uriApi,content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<bool>(r);
                return item;
            }
            else
            {
                error = "No se pudo insertar el dato";
                return false;
            }
        }

        private async Task<T> SearchById(string id)
        {
            HttpResponseMessage response = await client.GetAsync(uriApi+"/"+id).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<T>(content);
                return item;
            }
            else
            {
                error = "No se pudieron obtener los datos";
                return null;
            }
        }

        private async Task<bool> Update(T entidad) 
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(entidad), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PutAsync(uriApi, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<bool>(r);
                return item;
            }
            else
            {
                error = "No se pudo actualizar el dato";
                return false;
            }
        }

        private async Task<bool> Delete(string id)
        {
            HttpResponseMessage response = await client.DeleteAsync(uriApi + "/" + id).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<bool>(content);
                return item;
            }
            else
            {
                error = "No se pudo eliminar los datos";
                return false;
            }
        }

        internal async Task<IEnumerable<T>> TraerDatos(ConsultaAPIModel model)
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uriApi + "/consulta", content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
               //return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                //var item = JsonConvert.DeserializeObject<IEnumerable<T>>(r);
                //return item;
                var r = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<IEnumerable<T>>(r);
                return item;
            }
            else
            {
                error = "No se encontraron los datos";
                return null;
            }
        }

        public string Error => error;

        public IEnumerable<T> Leer => ObtenerDatos().Result;

        public bool Actualizar(T entidad) => Update(entidad).Result;

        public bool Agregar(T entidad) => Insertar(entidad).Result;

        public T BuscarPorId(string Id) => SearchById(Id).Result;

        public bool Eliminar(string Id) => Delete(Id).Result;
    }
}
