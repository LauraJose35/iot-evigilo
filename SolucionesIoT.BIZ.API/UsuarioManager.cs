﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolucionesIoT.BIZ.API
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuario
    {
        private async Task<Usuario> LoginAsyn(string correo, string password)
        {
            ConsultaAPIModel model = new ConsultaAPIModel()
            {
                NombreMetodo= "Login",
                Parametros = new List<string>() {correo, password}
            };
            var r = await TraerDatos(model).ConfigureAwait(false);
            var result = r.ToList();

            return result[0];
        }
        public Usuario Login(string correo, string password) => LoginAsyn(correo, password).Result;
    }
}
