﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolucionesIoT.BIZ.API
{
    public class SensorManager : GenericManager<Sensor>, ISensor
    {
        private async Task<IEnumerable<Sensor>> ListaDeSensoresPorCorreoAsync(string correo, string IdProyecto)
        {
            ConsultaAPIModel model = new ConsultaAPIModel() { 
            NombreMetodo= "ListaDeSensoresPorCorreo",
            Parametros= new List<string>() {correo, IdProyecto }
            };
            return await TraerDatos(model).ConfigureAwait(false);
        }

        private async Task<IEnumerable<Sensor>> ListaDeSensoresPorIdAsync(string idUsuario, string IdProyecto)
        {
            ConsultaAPIModel model = new ConsultaAPIModel()
            {
                NombreMetodo = "ListaDeSensoresPorId",
                Parametros = new List<string>() { idUsuario, IdProyecto }
            };
            // return await TraerDatos(model).ConfigureAwait(false);
            //var r = await TraerDatos(model).ConfigureAwait(false);
            //var result = r.ToList();
            //return r;
            return await TraerDatos(model).ConfigureAwait(false);
        }

        private async Task<Sensor> SensorPerteneceAUsuarioAsync(string idSensor, string idUsuario)
        {
            ConsultaAPIModel model = new ConsultaAPIModel()
            {
                NombreMetodo = "SensorPerteneceAUsuario",
                Parametros = new List<string>() {idSensor, idUsuario }
            };
            var r = await TraerDatos(model).ConfigureAwait(false);
            var result = r.ToList();

            return result[0];
        }

        public IEnumerable<Sensor> ListaDeSensoresPorCorreo(string correo, string IdProyecto) => ListaDeSensoresPorCorreoAsync(correo,IdProyecto).Result;

        public IEnumerable<Sensor> ListaDeSensoresPorId(string idUsuario, string IdProyecto) => ListaDeSensoresPorIdAsync(idUsuario,IdProyecto).Result;

        public Sensor SensorPerteneceAUsuario(string idSensor, string idUsuario) => SensorPerteneceAUsuarioAsync(idSensor, idUsuario).Result;
    }
}
