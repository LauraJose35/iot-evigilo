﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SolucionesIoT.BIZ.API
{
    public class ProyectoManager : GenericManager<Proyecto>, IProyecto
    {
        private async Task<IEnumerable<Proyecto>> ListaDeProyectosPorCorreoAsync(string correo)
        {
            ConsultaAPIModel model = new ConsultaAPIModel()
            {
                NombreMetodo = "ListaDeProyectosPorCorreo",
                Parametros = new List<string>() {correo }
            };
            return await TraerDatos(model).ConfigureAwait(false);
        }

        private async Task<IEnumerable<Proyecto>> ListaDeProyectosPorIdAsync(string id)
        {
            ConsultaAPIModel model = new ConsultaAPIModel() {
                NombreMetodo = "ListaDeProyectosPorId",
                Parametros= new List<string>() {id }
            };
            return await TraerDatos(model).ConfigureAwait(false);
        }
        public IEnumerable<Proyecto> ListaDeProyectosPorCorreo(string correo) => ListaDeProyectosPorCorreoAsync(correo).Result;

        public IEnumerable<Proyecto> ListaDeProyectosPorId(string id) => ListaDeProyectosPorIdAsync(id).Result;
    }
}
