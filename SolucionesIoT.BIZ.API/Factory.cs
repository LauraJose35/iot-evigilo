﻿using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionesIoT.BIZ.API
{
    public static class FabricManager
    {
        public static IAccion Accion => new AccionManager();
        public static ILectura Lectura => new LecturaManager();
        public static IProyecto Proyecto => new ProyectoManager();
        public static ISensor SensorManager => new SensorManager();
        public static IUsuario Usuario => new UsuarioManager();
    }
}
