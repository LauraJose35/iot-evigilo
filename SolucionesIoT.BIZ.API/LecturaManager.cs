﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SolucionesIoT.BIZ.API
{
    public class LecturaManager : GenericManager<Lectura>, ILectura
    {
        private async Task<IEnumerable<Lectura>> LecturasDelSensorAsyn(string id)
        {
            ConsultaAPIModel model = new ConsultaAPIModel()
            {
                NombreMetodo = "LecturasDelSensorId",
                Parametros = new List<string>() { id }
            };

            return await TraerDatos(model).ConfigureAwait(false);
        }

        private async Task<IEnumerable<Lectura>> LecturasDelSensorFechaAsync(string id, DateTime inicio, DateTime fin)
        {
            ConsultaAPIModel model = new ConsultaAPIModel()
            {
                NombreMetodo = "LecturasDelSensorFecha",
                Parametros = new List<string>() { id, inicio.ToString(), fin.ToString()}
            };

            return await TraerDatos(model).ConfigureAwait(false);
        }

        public IEnumerable<Lectura> LecturasDelSensorFecha(string id, DateTime inicio, DateTime fin) => LecturasDelSensorFechaAsync(id, inicio, fin).Result;
        
        public IEnumerable<Lectura> LecturasDelSensorId(string id) => LecturasDelSensorAsyn(id).Result;
    }
}
