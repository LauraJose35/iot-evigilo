﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ProgramaSimulacion
{
    public class Exportar
    {
        FileInfo file;
        StreamWriter text;
        public Exportar(string fileName)
        {
            FileName = fileName;
        }

        public string Error { get; private set; }
        public string FileName { get; private set; }

        public bool Crear()
        {
            try
            {
                file = new FileInfo(FileName);
                text = file.CreateText();
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool EscribirLinea(string linea)
        {
            try
            {
                text.WriteLine(linea);
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public void Cerrar()
        {
            text.Close();
        }

        public bool AbrirArchivo()
        {
            try
            {
                Process.Start(FileName);
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

    }
}