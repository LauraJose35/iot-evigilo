﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Microsoft.Win32;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProgramaSimulacion
{
    /// <summary>
    /// Lógica de interacción para Inicio.xaml
    /// </summary>
    public partial class Inicio : Window
    {
        public LoginSimulador Datos { get; set; }
        public IEnumerable<Sensor> ListaSensores { get; set; }
        ISensor repositorySensores;
        IUsuario repositoryUsuario;
        ILectura repositorioLectura;
        Sensor Model;
        List<Lectura> LecturasSimuladas;
        Exportar exportar;
        public Inicio(LoginSimulador Datos)
        {
            InitializeComponent();
            this.Datos = Datos;
            repositorySensores = FabricManager.SensorManager;
            repositoryUsuario = FabricManager.Usuario;
            repositorioLectura = FabricManager.Lectura;
            Model = DataContext as Sensor;
            ActualizarTabla();
            Title = "Proyecto: " + Datos.Proyecto.Nombre;
            HabilitarCajas(false);
        }

        #region Sensor        
        private void btnGuardarSensor_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombre.Text)&&string.IsNullOrEmpty(txtDescripcion.Text)&&string.IsNullOrEmpty(txtUnidad.Text)) {
                App.Error("Llene todos los campos");
                return;
            }
            Model.Id_Proyecto = Datos.Proyecto.Id;
            Model.Id_Usuario = Datos.Proyecto.Id_Usuario;
            if (repositorySensores.Agregar(Model))
            {
                App.Guardar("El sensor se ha almacenado correctamente");
                ActualizarTabla();
                LimpiarDatos();
            }
            else
            {
                 App.Error(repositorySensores.Error);
            }
        }

        private void ActualizarTabla()
        {
            dtgDatos.ItemsSource = null;
            dtgDatos.ItemsSource = repositorySensores.ListaDeSensoresPorId(Datos.Proyecto.Id_Usuario, Datos.Proyecto.Id);
            cmbSensores.ItemsSource = null;
            cmbSensores.ItemsSource = repositorySensores.ListaDeSensoresPorId(Datos.Proyecto.Id_Usuario, Datos.Proyecto.Id);
        }

        private void btnEliminarSensor_Click(object sender, RoutedEventArgs e)
        {
            Model = DataContext as Sensor;
            if (App.Eliminar("¿Desea eliminar este sensor " + Model.Nombre) == MessageBoxResult.OK)
            {
                if (repositorySensores.Eliminar(Model.Id))
                {
                    App.EliminarSimple("Sensor eliminado correctamente");
                    ActualizarTabla();
                }
                else
                    App.Error(repositorySensores.Error);
                LimpiarDatos();
            }
        }

        private void LimpiarDatos()
        {          
            txtDescripcion.Clear();
            txtNombre.Clear();
            txtUnidad.Clear();
        }

        private void btnActualizarSensor_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombre.Text) && string.IsNullOrEmpty(txtDescripcion.Text) && string.IsNullOrEmpty(txtUnidad.Text))
            {
                App.Error("Llene todos los campos");
                return;
            }
            Model = DataContext as Sensor;
            if (repositorySensores.Actualizar(Model))
            {
                App.Guardar("El sensor se ha actualizado correctamente");
                ActualizarTabla();
                LimpiarDatos();
            }
            else
                App.Error(repositorySensores.Error);
        }
        
   
        private void dtgDatos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Sensor sensorSeleccionado =  dtgDatos.SelectedItem as Sensor;
            if (sensorSeleccionado != null)
            {
                DataContext = sensorSeleccionado;
            }
        }
        #endregion

        #region Lecturas
        private void btnAgregarLecturas_Click(object sender, RoutedEventArgs e)
        {
            if(rAlmacenaDB.IsChecked==false && rCsv.IsChecked == false)
            {
                App.Error("Debe de seleccionar una opcion antes de guardar \n-Almacenar en Base de Datos o \n-Generar archivo csv");
                return;
            }
            Sensor sensorSeleccionado = cmbSensores.SelectedItem as Sensor;
            if (cmbSensores.SelectedItem != null)
            {
               if (rdRandom.IsChecked == true)
                {
                    int y = 0;
                    if (int.TryParse(txtNumDeValores.Text, out y) == true)
                    {
                        for (int i = 0; i < int.Parse(txtNumDeValores.Text); i++)
                        {
                            Random random = new Random();
                          
                            LecturasSimuladas.Add(new Lectura()
                            {
                                Id_Sensor = sensorSeleccionado.Id,
                                Valor = random.Next(1, 50).ToString()
                        });
                        }
                    }
                    else
                    {
                        App.Error("El número de valor de lecturas a agregar debe de ser númerico");
                    }
                }
                else
                {
                    if (LecturasSimuladas == null)
                    {
                        App.Eliminar("No cuenta con ningun valor agregado");
                        return;
                    }
                }
                if (rAlmacenaDB.IsChecked == true)
                {
                    foreach (var item in LecturasSimuladas)
                    {
                        repositorioLectura.Agregar(item);
                    }
                    App.Guardar("Las lecturas se almacenaron correctamente en la Base de Datos");
                }
                                
                if (rCsv.IsChecked == true)
                {
                    AlmacenarEnExcel();
                }
                txtNumDeValores.Clear();
                txtValor.Clear();
                HabilitarCajas(false) ;
                rdRandom.IsChecked = false;
                rAlmacenaDB.IsChecked = false;
                rCsv.IsChecked = false;
            }
            else
            {
                App.Error("Seleccione su sensor");
            }               
        }

        private void AlmacenarEnExcel()
        {
            try
            {
                Sensor sensorSeleccionado = cmbSensores.SelectedItem as Sensor;
                SaveFileDialog saveFileFialog = new SaveFileDialog()
                {
                    Title = "Guardar archivo CSV",
                    Filter = "csv files (*.csv)|*.csv"
                };
                if (saveFileFialog.ShowDialog().Value && saveFileFialog.FileName.Length > 0)
                {
                    exportar = new Exportar(saveFileFialog.FileName);
                    if (exportar.Crear())
                    {
                        if (!exportar.EscribirLinea($"Fecha y hora, Nombre del Sensor, {sensorSeleccionado.UnidadMedida}"))
                        {
                            App.Error(exportar.Error);
                            return;
                        }
                       
                        foreach (var lecturas in LecturasSimuladas)
                        {
                            if (!exportar.EscribirLinea(lecturas.FechaHora + "," + sensorSeleccionado.Nombre + "," + lecturas.Valor))
                            {
                                App.Error(exportar.Error);
                                return;
                            }
                        }
                        exportar.Cerrar();
                       // exportar.AbrirArchivo();
                    }
                    else
                    {
                        App.Error(exportar.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                App.Error(ex.Message);
            }
        }

        private void btnCancelarSensor_Click(object sender, RoutedEventArgs e)
        {
            LimpiarDatos();
        }

        private void btnNuevoLecturas_Click(object sender, RoutedEventArgs e)
        {
            LecturasSimuladas = new List<Lectura>();
            HabilitarCajas(true);
            txtNumDeValores.IsEnabled = false;
        }

        private void HabilitarCajas(bool v)
        {
            txtNumDeValores.IsEnabled = v;
            rdRandom.IsEnabled = v;
            rAlmacenaDB.IsEnabled = v;
            cmbSensores.IsEnabled = v;
            txtValor.IsEnabled = v;
            btnAgregarLecturas.IsEnabled = v;
            rCsv.IsEnabled = v;
            btnNuevoLecturas.IsEnabled = !v;
        }


        private void rdRandom_Unchecked(object sender, RoutedEventArgs e)
        {
            txtNumDeValores.IsEnabled = false;
            txtValor.IsEnabled = true;
        }

        private void rdRandom_Checked(object sender, RoutedEventArgs e)
        {
            txtNumDeValores.IsEnabled = true;
            txtValor.IsEnabled = false;
        }

        private void btnAgregarValor_Click(object sender, RoutedEventArgs e)
        {
            Sensor sensorSeleccionado = cmbSensores.SelectedItem as Sensor;
            if (cmbSensores.SelectedItem != null)
            {
                LecturasSimuladas.Add(new Lectura()
                {
                    Id_Sensor = sensorSeleccionado.Id,
                    Valor = txtValor.Text
                });
                txtValor.Clear();
                txtValor.Focus();
                App.Guardar("Valor guardado");
            }
        }
        #endregion
    }
}
