﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProgramaSimulacion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IUsuario repositoryUsuario;
        IProyecto repositoryProyecto;
        public LoginSimulador Login { get; set; }
        public Proyecto proyecto { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            repositoryUsuario = FabricManager.Usuario;
            repositoryProyecto = FabricManager.Proyecto;
            Login = DataContext as LoginSimulador;
        }

       

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (repositoryUsuario.Login(Login.Correo, Login.Password) != null)
                {
                    proyecto = repositoryProyecto.BuscarPorId(Login.IdProyecto);
                    Login.Proyecto = proyecto;
                    if (proyecto != null)
                    {
                        Inicio inicio = new Inicio(Login);
                        inicio.Show();
                        this.Close();
                    }
                }
                else
                {
                    if (repositoryUsuario.Login(Login.Correo, Login.Password) == null)
                        App.Error("Correo o contraseña incorrecta");

                    proyecto = repositoryProyecto.BuscarPorId(Login.IdProyecto);
                    Login.Proyecto = proyecto;

                    if (proyecto == null)
                        App.Error("El Id del proyecto no existe, verifique");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
    }
}
