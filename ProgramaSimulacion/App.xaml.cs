﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ProgramaSimulacion
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static void Error(string error)=> MessageBox.Show(error,"Error", MessageBoxButton.OK, MessageBoxImage.Error);

        public static void Guardar(string guardar) => MessageBox.Show(guardar,"Guardar", MessageBoxButton.OK, MessageBoxImage.Information);

        public static void Editar() => MessageBox.Show("La información se guardo con exito", "Editar", MessageBoxButton.OK, MessageBoxImage.Information);
        public static MessageBoxResult Eliminar(string eliminar)=> MessageBox.Show(eliminar, "Eliminar", MessageBoxButton.OKCancel, MessageBoxImage.Question);
        public static void EliminarSimple(string eliminar) => MessageBox.Show(eliminar, "Eliminar", MessageBoxButton.OK, MessageBoxImage.Information);

        public static bool MensajeInterrogrativo(string mensaje)=>MessageBox.Show(mensaje, "Archivo excel", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
        
    }
}
