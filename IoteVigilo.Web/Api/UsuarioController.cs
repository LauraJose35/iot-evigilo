﻿using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : GenericApiController<Usuario>
    {
        static IUsuario manager = Factory.UsuarioManager();
        public UsuarioController() : base(manager)
        {
        }

        public override ActionResult<IEnumerable<Usuario>> Consulta([FromBody] ConsultaAPIModel model, string id)
        {
            try
            {
                List<Usuario> datos = null;
                if (model.NombreMetodo == "Login")
                {
                    datos = new List<Usuario>();
                    datos.Add(manager.Login(model.Parametros[0], model.Parametros[1]));
                    return Ok(datos);
                } else
                {
                    return BadRequest("Nombre del método no encontrado");                
                }
            }
            catch (Exception)
            {
                return BadRequest("Error al procesar los datos");               
            }           
        }
    }
}
