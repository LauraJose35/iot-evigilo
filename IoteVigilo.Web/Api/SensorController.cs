﻿using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorController : GenericApiController<Sensor>
    {
        static ISensor manager = Factory.SensorManager();
        public SensorController() : base(manager)
        {
        }

        public override ActionResult<IEnumerable<Sensor>> Consulta([FromBody] ConsultaAPIModel model, string id)
        {
            try
            {
                if (model.NombreMetodo == "SensorPerteneceAUsuario")
                {
                    Sensor dato;
                    dato = manager.SensorPerteneceAUsuario(model.Parametros[0],model.Parametros[1]);
                    return Ok(dato);
                }
                else
                {
                    List<Sensor> datos;
                    switch (model.NombreMetodo)
                    {
                        case "ListaDeSensoresPorCorreo":
                            datos = manager.ListaDeSensoresPorCorreo(model.Parametros[0], model.Parametros[1]).ToList();
                            break;
                        case "ListaDeSensoresPorId":
                            datos = manager.ListaDeSensoresPorId(model.Parametros[0], model.Parametros[1]).ToList();
                            break;
                        default:
                            datos = null;
                            break;
                    }
                    if (datos == null)
                    {
                        return BadRequest("Nombre de método no encontrado");
                    }
                    else
                    {
                        return Ok(datos);
                    }
                }
            }
            catch (Exception)
            {
                return BadRequest("Error al procesar los datos");
            }
        }
    }
}
