﻿using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturaController : GenericApiController<Lectura>
    {
        static ILectura manager = Factory.LecturaManager();
        public LecturaController() : base(manager)
        {
        }

        public override ActionResult<IEnumerable<Lectura>> Consulta([FromBody] ConsultaAPIModel model, string id)
        {
            try
            {
                List<Lectura> datos;
                switch (model.NombreMetodo)
                {
                    case "LecturasDelSensorId":
                        datos = manager.LecturasDelSensorId((string)model.Parametros[0]).ToList();
                        break;
                    case "LecturasDelSensorFecha":
                        datos = manager.LecturasDelSensorFecha((string)model.Parametros[0], DateTime.Parse(model.Parametros[1]), DateTime.Parse(model.Parametros[2])).ToList();
                        break;
                    default:
                        datos = null;
                        break;
                }
                if (datos == null)
                {
                    return BadRequest("Nombre del método no encontrado");
                }
                else
                {
                    return Ok(datos);
                }
            }
            catch (Exception)
            {
                return BadRequest("Error al procesar los datos...");
            }
           
        }
    }
}
