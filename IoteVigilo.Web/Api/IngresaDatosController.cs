﻿using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngresaDatosController : ControllerBase
    {
        IUsuario usuarioManager;
        ILectura lecturaManager;
        ISensor sensorManager;
        IAccion accionManager;
        public IngresaDatosController()
        {
            usuarioManager = Factory.UsuarioManager();
            lecturaManager = Factory.LecturaManager();
            sensorManager = Factory.SensorManager();
            accionManager = Factory.AccionManager();
        }

        [HttpPut]
        public ActionResult<Accion> Put(string correo, string password, string IdSensor, string actuador, string estado) 
        {
            Usuario u = usuarioManager.Login(correo, password);
            if (u != null)
            {
                Sensor sensor = sensorManager.SensorPerteneceAUsuario(IdSensor, u.Id);
                if (sensor!=null)
                {
                    try
                    {
                        return Ok(accionManager.Agregar(new Accion()
                        {
                            Id_Sensor = IdSensor,
                            Actuador = actuador,
                            Estado=estado
                        }));
                    }
                    catch (Exception)
                    {
                        return BadRequest("No se pudo crear la Accion" + lecturaManager.Error);
                    }
                }
                else
                {
                    return BadRequest("Sensor no pertenece al usuario");
                }
            }
            else
            {
                return BadRequest("Usuario y/o contraseña incorrecta..");
            }
        }

        [HttpPost]
        public ActionResult<Lectura> Post(string correo, string password, string IdSensor, string valor)
        {
            Usuario u = usuarioManager.Login(correo, password);
            if (u != null)
            {
                Sensor sensor = sensorManager.SensorPerteneceAUsuario(IdSensor, u.Id);
                if (sensor!=null) 
                {
                    try
                    {
                        return Ok(lecturaManager.Agregar(new Lectura()
                        {
                            Id_Sensor = IdSensor,
                            Valor = valor,
                        }));
                    }
                    catch (Exception)
                    {
                        return BadRequest("No se pudo crear la Lectura"+ lecturaManager.Error);
                    }
                } 
                else 
                {
                    return BadRequest("Sensor no pertenece al usuario");
                }
            }
            else 
            {
                return BadRequest("Usuario y/o contraseña incorrecta..");
            }
        }
    }
}
