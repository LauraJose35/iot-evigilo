﻿using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProyectoController : GenericApiController<Proyecto>
    {
        static IProyecto manager = Factory.ProyectoManager();
        public ProyectoController() : base(manager)
        {
        }

        public override ActionResult<IEnumerable<Proyecto>> Consulta([FromBody] ConsultaAPIModel model, string id)
        {
            try
            {
                List<Proyecto> datos;
                switch (model.NombreMetodo)
                {
                    case "ListaDeProyectosPorCorreo":
                        datos = manager.ListaDeProyectosPorCorreo(model.Parametros[0]).ToList();
                        break;
                    case "ListaDeProyectosPorId":
                        datos = manager.ListaDeProyectosPorId(model.Parametros[0]).ToList();
                        break;
                    default:
                        datos = null;
                        break;
                }
                if (datos == null) 
                {
                    return BadRequest("Nombre del método no encontrado");
                } else 
                {
                    return Ok(datos);
                }
            }
            catch (Exception)
            {
                return BadRequest("Error al procesar los datos...");
            }           
        }
    }
}
