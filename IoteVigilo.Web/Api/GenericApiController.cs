﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IoteVigilo.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericApiController<T> : ControllerBase where T:BaseDTO
    {
        IGenericManager<T> manager;
        public GenericApiController(IGenericManager<T> genericManager)
        {
            manager = genericManager;
        }
        // GET: api/<GenericApiController>
        [HttpGet]
        public ActionResult<IEnumerable<T>> Get()
        {
            try
            {
                return Ok(manager.Leer);
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // GET api/<GenericApiController>/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                return Ok(manager.BuscarPorId(id));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // POST api/<GenericApiController>
        [HttpPost]
        public ActionResult<bool> Post([FromBody] T value)
        {
            try
            {
                return Ok(manager.Agregar(value));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // PUT api/<GenericApiController>/5
        [HttpPut]
        public ActionResult<bool> Put( [FromBody] T value)
        {
            try
            {
                return Ok(manager.Actualizar(value));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // DELETE api/<GenericApiController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {
                return Ok(manager.Eliminar(id));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        [HttpPost("{id}")]
        public abstract ActionResult<IEnumerable<T>> Consulta([FromBody] ConsultaAPIModel model, string id);
    }
}
