using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class EliminaSensorModel : PageModel
    {
        [BindProperty]
        public Sensor SensorActuador { get; set; }
        [BindProperty]
        public string Mensaje { get; set; }
        [BindProperty]
        public string IdUser { get; set; }
        public void OnGet(string idSensor)
        {
            SensorActuador = FabricManager.SensorManager.BuscarPorId(idSensor);
            Mensaje = "";
        }
        public async Task<IActionResult> OnPost()
        {
            IdUser = SensorActuador.Id_Usuario;
            if (FabricManager.SensorManager.Eliminar(SensorActuador.Id)) {
                return RedirectToPage("/DetalleProyecto",new { idProyecto =SensorActuador.Id_Proyecto});
            }
            else 
            {
                Mensaje = "Error al eliminar el sensor";
                return Page();
            }
        }
    }
}
