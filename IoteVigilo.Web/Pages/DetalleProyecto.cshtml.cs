using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class DetalleProyectoModel : PageModel
    {
        [BindProperty]
        public Proyecto Proyecto { get; set; }

        [BindProperty]
        public IEnumerable<Sensor> SensorActuadors { get; set; }
        public void OnGet(string idProyecto)
        {
            //Proyecto = Factory.ProyectoManager().BuscarPorId(idProyecto);
            //SensorActuadors = Factory.SensorManager().ListaDeSensoresPorId(Proyecto.Id_Usuario, Proyecto.Id).ToList();
            Proyecto = FabricManager.Proyecto.BuscarPorId(idProyecto);
            SensorActuadors = FabricManager.SensorManager.ListaDeSensoresPorId(Proyecto.Id_Usuario, Proyecto.Id).ToList();
        }
    }
}
