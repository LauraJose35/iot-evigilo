using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class EliminaProyectoModel : PageModel
    {
        [BindProperty]
        public Proyecto Proyecto { get; set; }

        [BindProperty]
        public string Mensaje { get; set; }
        [BindProperty]
        public string IdUser { get; set; }
        public void OnGet(string id)
        {
            Proyecto = FabricManager.Proyecto.BuscarPorId(id);
            Mensaje = "";
        }

        public async Task<IActionResult> OnPost()
        {
            IdUser = Proyecto.Id_Usuario;
            if (FabricManager.Proyecto.Eliminar(Proyecto.Id))
            {
                return RedirectToPage("/PanelPrincipal", new { IdUsuario = IdUser });
            }
            else
            {
                Mensaje = "Error al eliminar el proyecto";
                return Page();
            }
        }
    }
}
