using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoteVigilo.Web.Reporte;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class DetalleSensorModel : PageModel
    {
        [BindProperty]
        public Sensor SensorActuador { get; set; }
        [BindProperty]
        public List<Lectura> Lectura { get; set; }
        [BindProperty]
        public string IdSensor { get; set; }
        [BindProperty]
        public List<LecturasModel> lecturas { get; set; }
        ReporteLectura Reporte;

        public void OnGet(string idSensor)
        {
            SensorActuador = FabricManager.SensorManager.BuscarPorId(idSensor);
            Lectura = FabricManager.Lectura.LecturasDelSensorId(idSensor).OrderBy(p => p.FechaHora).ToList();
            IdSensor = idSensor;
        }
              
        public IActionResult OnPost()
        {
            try
            {
                SensorActuador = FabricManager.SensorManager.BuscarPorId(IdSensor);
                Lectura = FabricManager.Lectura.LecturasDelSensorId(IdSensor).OrderBy(p => p.FechaHora).ToList();
                foreach (var item in Lectura)
                {
                    lecturas.Add(new LecturasModel()
                    {
                        FechaHora = item.FechaHora,
                        Valor = item.Valor,
                        UnidadValor = SensorActuador.UnidadMedida,
                    });
                }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine($"Lecturas del sensor {SensorActuador.Nombre}");
                stringBuilder.AppendLine("Id,Fecha hora,Valor,Unidad de medida");
                foreach (var item in Lectura)
                {
                    stringBuilder.AppendLine ($"{item.Id},{ item.FechaHora},{ item.Valor},{SensorActuador.UnidadMedida}");
                }
                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "Lecturas.csv");
            }
            catch
            {
                return Page();
            }
        }
    }
}
