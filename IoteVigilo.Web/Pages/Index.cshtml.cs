﻿using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SolucionesIoT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        [BindProperty]
        public Login Login { get; set; }
        [BindProperty]
        public bool EsLogin { get; set; }
        [BindProperty]
        public string Error { get; set; }
        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPost()
        {
            try
            {
                if (EsLogin)
                {
                    IUsuario usuarioManager = FabricManager.Usuario;
                    Usuario u = usuarioManager.Login(Login.Correo, Login.Password);
                    if (u != null)
                    {
                        List<Claim> claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Name, u.Nombre),
                            new Claim(ClaimTypes.Email, u.Email)
                        };
                        ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");
                        ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                        await HttpContext.SignInAsync(scheme: "Seguridad", principal: principal, properties: new AuthenticationProperties
                        {
                            ExpiresUtc = DateTime.UtcNow.AddMinutes(10)
                        });

                        //List<Claim> claims = new List<Claim>
                        //{
                        //    new Claim(ClaimTypes.Name, u.Nombre),
                        //    new Claim(ClaimTypes.Email, u.Email)
                        //};

                        //// create identity
                        //ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

                        //// create principal
                        //ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                        //// sign-in
                        //await HttpContext.SignInAsync(
                        //        scheme: "FiverSecurityCookie",
                        //        principal: principal);

                        return RedirectToPage("/PanelPrincipal", new { IdUsuario=u.Id});
                    }
                    else
                    {
                        Error = "Usuario o contraseña incorrecto";
                        return Page();
                    }
                }
                else
                {
                    return RedirectToPage("/NuevoUsuario");
                }
            }
            catch (Exception ex)
            {
                return Page();
            }
           
        }
    }
}
