using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    public class NuevoUsuarioModel : PageModel
    {
        [BindProperty]
        public Usuario Usuario { get; set; }
        public string Mensaje { get; set; }
        public void OnGet()
        {
            Usuario = new Usuario();
            Mensaje = "";
        }

        public async Task<ActionResult> OnPost()
        {
            if (FabricManager.Usuario.Agregar(Usuario))
            {
                return RedirectToPage("/Index");
            }
            else
            {
                Mensaje = "Error al crear al usuario";
                return Page();
            }
        }
    }
}
