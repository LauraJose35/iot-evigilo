using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class NuevoProyectoModel : PageModel
    {
        [BindProperty]
        public Proyecto Proyecto { get; set; }
       
        public string Mensaje{ get; set; }
        public void OnGet(string id)
        {
            Proyecto = new Proyecto();

            Proyecto.Id_Usuario = id;
            Mensaje = "";
        }

        public async Task<ActionResult> OnPost() 
        {
            if (FabricManager.Proyecto.Agregar(Proyecto))
            {
                return RedirectToPage("/PanelPrincipal", new { IdUsuario = Proyecto.Id_Usuario });
            }
            else {
                Mensaje = "Error al crear un nuevo proyecto";
                return Page();
            }
        }
    }
}
