using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Entidades;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class NuevoSensorModel : PageModel
    {
        [BindProperty]
        public Sensor SensorActuador { get; set; }
        public string Mensaje { get; set; }
        [BindProperty]
        public Proyecto Proyecto { get; set; }
        public void OnGet(string idProyecto)
        {
            SensorActuador = new Sensor();
            Proyecto = FabricManager.Proyecto.BuscarPorId(idProyecto);
            SensorActuador.Id_Proyecto = Proyecto.Id;
            SensorActuador.Id_Usuario = Proyecto.Id_Usuario;
            Mensaje = "";
        }

        public async Task<ActionResult> OnPost() {
            string IdProyecto = SensorActuador.Id_Proyecto;
            if (FabricManager.SensorManager.Agregar(SensorActuador))
            {
                return RedirectToPage("/DetalleProyecto", new { idProyecto =IdProyecto});
            }
            else
            {
                Mensaje = "Error al crear un nuevo proyecto";
                return Page();
            }
        }
    }
}
