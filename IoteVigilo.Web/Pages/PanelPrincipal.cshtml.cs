using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionesIoT.BIZ.API;

namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class PanelPrincipalModel : PageModel
    {
        [BindProperty]
        public PanelUsuarioModel DatosPanelUsuario { get; set; }
        public void OnGet(string IdUsuario)
        {
            DatosPanelUsuario = new PanelUsuarioModel();
            DatosPanelUsuario.Usuario = Factory.UsuarioManager().BuscarPorId(IdUsuario);
            DatosPanelUsuario.Proyectos = Factory.ProyectoManager().ListaDeProyectosPorId(IdUsuario).ToList();
            //DatosPanelUsuario.Usuario = FabricManager.Usuario.BuscarPorId(IdUsuario);
            //DatosPanelUsuario.Proyectos = FabricManager.Proyecto.ListaDeProyectosPorId(IdUsuario).ToList();
        }
    }
}
