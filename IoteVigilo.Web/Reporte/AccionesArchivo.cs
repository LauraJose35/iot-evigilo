﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Reporte
{
    public class AccionesArchivo
    {
        public string Archivo { get; set; }
        public AccionesArchivo(string archivo= @"C:/8 semestre/bd/archivo.txt")
        {
            Archivo = archivo;
        }

        public string Lectura()
        {
            try
            {
                StreamReader fila = new StreamReader(Archivo);
                string elementos = fila.ReadToEnd();
                fila.Close();
                return elementos;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public bool Exportar(string datos)
        {
            try
            {
                StreamWriter archivo = new StreamWriter(Archivo);
                archivo.WriteLine(datos);
                archivo.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
