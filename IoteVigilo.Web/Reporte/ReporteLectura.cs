﻿using IotVigilio.COMMON.Entidades;
using iTextSharp.text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace IoteVigilo.Web.Reporte
{
    public class ReporteLectura
    {
        FileInfo file;
        StreamWriter text;//@"C:/8 semestre/bd/Lecturas.csv
        public ReporteLectura(string fileName = @"C:/Lecturas/Lecturas del sensor.csv")
        {
            FileName = fileName;
        }

        public string FileName { get; private set; }
        public string Error { get; private set; }

        public bool Crear()
        {
            try
            {
                file = new FileInfo(FileName);
                text = file.CreateText();
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool EscribirLinea(string linea)
        {
            try
            {
                text.WriteLine(linea);
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public void Cerrar()
        {
            text.Close();
        }

        public bool AbrirArchivo()
        {
            try
            {
                Process.Start(FileName);
                Error = "";
                return true;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

    }
}
