﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IotVigilio.BIZ
{
    public class UsuarioManager : GenericManager<Usuario>, IUsuario
    {
        public UsuarioManager(IGenericRepository<Usuario> repositorio) : base(repositorio)
        {
        }

        public Usuario Login(string correo, string password) => repository.Read.Where(p=>p.Email==correo && p.Password==password).SingleOrDefault();
    }
}
