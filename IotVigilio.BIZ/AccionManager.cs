﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.BIZ
{
    public class AccionManager : GenericManager<Accion>, IAccion
    {
        public AccionManager(IGenericRepository<Accion> repositorio) : base(repositorio)
        {
        }
    }
}
