﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Validadores;
using IotVigilio.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.BIZ
{
    public static class Factory
    {
        public static LecturaManager LecturaManager() => new LecturaManager(new GenericRepository<Lectura>(new GenericValidator<Lectura>()));
        public static ProyectoManager ProyectoManager() => new ProyectoManager(new GenericRepository<Proyecto>(new GenericValidator<Proyecto>()),new GenericRepository<Usuario>(new GenericValidator<Usuario>()));
        public static SensorManager SensorManager() => new SensorManager(new GenericRepository<Sensor>(new GenericValidator<Sensor>()), new GenericRepository<Usuario>(new GenericValidator<Usuario>()), new GenericRepository<Proyecto>(new GenericValidator<Proyecto>()));
        public static UsuarioManager UsuarioManager() => new UsuarioManager(new GenericRepository<Usuario>(new GenericValidator<Usuario>()));
        public static AccionManager AccionManager() => new AccionManager(new GenericRepository<Accion>(new GenericValidator<Accion>()));
    }
}
