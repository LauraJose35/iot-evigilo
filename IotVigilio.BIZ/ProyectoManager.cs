﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IotVigilio.BIZ
{
    public class ProyectoManager : GenericManager<Proyecto>, IProyecto
    {
        IGenericRepository<Usuario> usuarioRepository;
        public ProyectoManager(IGenericRepository<Proyecto> repositorio, IGenericRepository<Usuario> usuarioRepository) : base(repositorio)
        {
            this.usuarioRepository = usuarioRepository;
        }

        public IEnumerable<Proyecto> ListaDeProyectosPorCorreo(string correo)
        {
            Usuario u = usuarioRepository.Read.Where(p=>p.Email==correo).SingleOrDefault();
            return repository.Read.Where(p=>p.Id_Usuario==u.Id).ToList();
        }

        public IEnumerable<Proyecto> ListaDeProyectosPorId(string idUsuario)
        {
            Usuario u = usuarioRepository.Read.Where(u => u.Id == idUsuario).SingleOrDefault();
            return repository.Read.Where(u=>u.Id_Usuario==idUsuario).ToList();
        }
    }
}
