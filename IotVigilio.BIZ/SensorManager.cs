﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IotVigilio.BIZ
{
    public class SensorManager : GenericManager<Sensor>, ISensor
    {
        IGenericRepository<Usuario> usuarioRepository;
        IGenericRepository<Proyecto> proyectoRepository;
        public SensorManager(IGenericRepository<Sensor> repositorio, IGenericRepository<Usuario> usuarioRepository, IGenericRepository<Proyecto> proyectoRepository) : base(repositorio)
        {
            this.usuarioRepository = usuarioRepository;
            this.proyectoRepository = proyectoRepository;
        }

        public IEnumerable<Sensor> ListaDeSensoresPorCorreo(string correo, string IdProyecto)
        {
            Usuario u = usuarioRepository.Read.Where(u => u.Email == correo).SingleOrDefault();
            Proyecto p = proyectoRepository.Read.Where(p=>p.Id==IdProyecto).SingleOrDefault();
            return repository.Read.Where(s=>s.Id_Usuario==u.Id&&s.Id_Proyecto==IdProyecto);/*ListaDeSensoresPorCorreo(u.Email,p.Id);*/
        }

        public IEnumerable<Sensor> ListaDeSensoresPorId(string idUsuario, string IdProyecto)
        {
            //Usuario u = usuarioRepository.Query(u => u.Id == id).SingleOrDefault();
            Proyecto p = proyectoRepository.Read.Where(p => p.Id == IdProyecto).SingleOrDefault();
            return repository.Read.Where(s=>s.Id_Usuario==idUsuario&&s.Id_Proyecto==IdProyecto); /*ListaDeSensoresPorCorreo(u.Id, p.Id);*/
        }

        public Sensor SensorPerteneceAUsuario(string idSensor, string idUsuario) => repository.Read.Where(s=>s.Id==idSensor && s.Id_Usuario==idUsuario).SingleOrDefault();
    }
}
