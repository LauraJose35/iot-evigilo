﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IotVigilio.BIZ
{
    public class LecturaManager : GenericManager<Lectura>, ILectura
    {
        public LecturaManager(IGenericRepository<Lectura> repositorio) : base(repositorio)
        {
        }

        public IEnumerable<Lectura> LecturasDelSensorId(string id)
        {
            return repository.Read.Where(s => s.Id_Sensor == id);
        }

        public IEnumerable<Lectura> LecturasDelSensorFecha(string id, DateTime inicio, DateTime fin)
        {
            return repository.Read.Where(s=>s.Id_Sensor==id && s.FechaHora>=inicio && s.FechaHora<=fin);
        }
    }
}
