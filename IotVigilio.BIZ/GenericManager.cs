﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IotVigilio.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> repository;
        public GenericManager(IGenericRepository<T> repositorio)
        {
            repository = repositorio;
        }
        public string Error { get { return repository.Error; } }

        public IEnumerable<T> Leer { get { return repository.Read; } }

        public bool Actualizar(T entidad) => repository.Update(entidad);

        public bool Agregar(T entidad) => repository.Create(entidad);

        public T BuscarPorId(string Id) => repository.Read.Where(e=>e.Id==Id).SingleOrDefault();

        public bool Eliminar(string Id) => repository.Delete(Id);
    }
}
