﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Entidades
{
    public class Accion:BaseDTO
    {
        public string Id_Sensor { get; set; }
        public string Actuador { get; set; }
        public string Estado { get; set; }
    }
}
