﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Entidades
{
    public class Lectura:BaseDTO
    {
        public string Id_Sensor { get; set; }
        //public string Id_Usuario { get; set; }
        public string Valor { get; set; }
    }
}
