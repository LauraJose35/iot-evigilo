﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Entidades
{
    public class Proyecto:BaseDTO
    {
        public string Foto { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Id_Usuario { get; set; }
    }
}
