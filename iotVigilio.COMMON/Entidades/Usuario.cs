﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace IotVigilio.COMMON.Entidades
{
    public class Usuario:BaseDTO
    {
        public ImageSource Foto { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

    }
}
