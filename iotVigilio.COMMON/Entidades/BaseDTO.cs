﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Entidades
{
    public abstract class BaseDTO:IDisposable
    {
        public string Id { get; set; }
        public DateTime FechaHora { get; set; }

        private bool _isDisponsable;
        public void Dispose()
        {
            if (!_isDisponsable) {
                this._isDisponsable = true;
                GC.SuppressFinalize(this);
            }
        }
    }
}
