﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Entidades
{
    public class Sensor:BaseDTO
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string UnidadMedida { get; set; }
        public string Id_Proyecto { get; set; }
        public string Id_Usuario { get; set; }
        public override string ToString()
        {
            return $"{Nombre} {UnidadMedida}";
        }
    }
}
