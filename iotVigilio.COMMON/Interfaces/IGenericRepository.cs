﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IotVigilio.COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        string Error { get; }
        IEnumerable<T> Read { get; }
        bool Create(T entidad);
        bool Update(T entidad);
        bool Delete(string Id);
        T SearchById(string Id);
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);
    }
}
