﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Interfaces
{
    public interface ISensor:IGenericManager<Sensor>
    {
        IEnumerable<Sensor> ListaDeSensoresPorId(string id, string IdProyecto);
        IEnumerable<Sensor> ListaDeSensoresPorCorreo(string correo, string IdProyecto);
        Sensor SensorPerteneceAUsuario(string idSensor, string idUsuario);
    }
}
