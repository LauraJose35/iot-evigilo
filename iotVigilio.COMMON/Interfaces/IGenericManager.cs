﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Interfaces
{
    public interface IGenericManager<T> where T:BaseDTO
    {
        string Error { get; }
        bool Agregar(T entidad);
        bool Actualizar(T entidad);
        bool Eliminar(string Id);
        IEnumerable<T> Leer { get; }
        T BuscarPorId(string Id);
    }
}
