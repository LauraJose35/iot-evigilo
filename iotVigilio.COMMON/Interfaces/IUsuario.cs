﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Interfaces
{
    public interface IUsuario:IGenericManager<Usuario>
    {
        Usuario Login(string correo, string password);
    }
}
