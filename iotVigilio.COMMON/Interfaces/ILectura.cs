﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Interfaces
{
    public interface ILectura:IGenericManager<Lectura>
    {
        IEnumerable<Lectura> LecturasDelSensorId(string id);
        IEnumerable<Lectura> LecturasDelSensorFecha(string id, DateTime inicio, DateTime fin);
    }
}
