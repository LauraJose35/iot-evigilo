﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Interfaces
{
    public interface IProyecto:IGenericManager<Proyecto>
    {
        IEnumerable<Proyecto> ListaDeProyectosPorId(string id);
        IEnumerable<Proyecto> ListaDeProyectosPorCorreo(string correo);
    }
}
