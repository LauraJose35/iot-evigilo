﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using IotVigilio.COMMON.Entidades;

namespace IotVigilio.COMMON.Validadores
{
    public class ProyectoValidator:GenericValidator<Proyecto>
    {
        public ProyectoValidator()
        {
            RuleFor(e=>e.Descripcion).NotNull().MaximumLength(100);
            RuleFor(e=>e.Nombre).NotEmpty().MaximumLength(25).WithMessage("Falta agregar el nombre del Proyecto");
            RuleFor(e => e.Id_Usuario).NotEmpty().NotNull();
        }
    }
}
