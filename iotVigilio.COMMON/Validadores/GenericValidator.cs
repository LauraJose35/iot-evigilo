﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using IotVigilio.COMMON.Entidades;

namespace IotVigilio.COMMON.Validadores
{
    public class GenericValidator<T>:AbstractValidator<T> where T: BaseDTO
    {
        public GenericValidator()
        {
            RuleFor(e=>e.FechaHora).NotEmpty();
            RuleFor(e=>e.Id).NotEmpty();
        }
    }
}
