﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using IotVigilio.COMMON.Entidades;

namespace IotVigilio.COMMON.Validadores
{
    public class UsuarioValidator:GenericValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(e=>e.ApellidoMaterno).NotEmpty().MaximumLength(25);
            RuleFor(e=>e.ApellidoPaterno).NotEmpty().MaximumLength(25);
            RuleFor(e=>e.Email).NotEmpty().EmailAddress().WithMessage("Falta agregar el gmail");
            RuleFor(e=>e.Nombre).NotEmpty().MaximumLength(25);
            RuleFor(e => e.Password).NotEmpty().MaximumLength(15).MinimumLength(5).WithMessage("El password debe de ser de un rango de caracteres de 5 a 15");
        }
    }
}
