﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using IotVigilio.COMMON.Entidades;

namespace IotVigilio.COMMON.Validadores
{
    public class SensorValidator:GenericValidator<Sensor>
    {
        public SensorValidator()
        {
            RuleFor(e=>e.Id_Proyecto).NotEmpty();
            RuleFor(e=>e.Id_Usuario).NotEmpty();
            RuleFor(e=>e.Nombre).NotEmpty().MaximumLength(25).WithMessage("Falta agregar el nombre del sensor");
            RuleFor(e=>e.UnidadMedida).NotEmpty().WithMessage("Falta agregar la unidade de medida del sensor");
        }
    }
}
