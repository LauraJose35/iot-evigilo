﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using IotVigilio.COMMON.Entidades;

namespace IotVigilio.COMMON.Validadores
{
    public class AccionValidator : GenericValidator<Accion>
    {
        public AccionValidator()
        {
            RuleFor(e => e.Id_Sensor).NotEmpty();
            RuleFor(e => e.Estado).NotEmpty();
            RuleFor(e => e.Actuador).NotNull();
        }
    }
}
