﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
namespace IotVigilio.COMMON.Validadores
{
    public class LecturaValidator:GenericValidator<Lectura>
    {
        public LecturaValidator()
        {
            RuleFor(e => e.Id_Sensor).NotEmpty();
            //RuleFor(e => e.Id_Usuario).NotEmpty();
            RuleFor(e=>e.Valor).NotNull();
        }
    }
}
