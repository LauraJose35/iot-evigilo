﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Modelos
{
    public class LecturasModel
    {
        public DateTime FechaHora { get; set; }
        public string Valor { get; set; }
        public string UnidadValor { get; set; }

        public override string ToString()
        {
            return $"{FechaHora}, {Valor}, {UnidadValor}";
        }
    }
}
