﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Modelos
{
    public class Login
    {
        public string Correo { get; set; }
        public string Password { get; set; }
    }
}
