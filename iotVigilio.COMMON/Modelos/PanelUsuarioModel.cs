﻿using IotVigilio.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Modelos
{
    public class PanelUsuarioModel
    {
        public List<Proyecto> Proyectos { get; set; }
        public Proyecto ProyectoSeleccionado { get; set; }
        public Usuario Usuario { get; set; }
        public List<Lectura> LecturasDelSensor { get; set; }
        public PanelUsuarioModel()
        {
            Usuario = new Usuario();
            ProyectoSeleccionado = new Proyecto();
        }
    }
}
