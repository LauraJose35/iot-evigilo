﻿using IotVigilio.COMMON.Entidades;
using IotVigilio.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Modelos
{
    public class LoginSimulador
    {
        public string IdProyecto { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        public Proyecto Proyecto { get; set; }
    }
}
