﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IotVigilio.COMMON.Modelos
{
    public class ConsultaAPIModel
    {
        public string NombreMetodo { get; set; }
        public List<string> Parametros { get; set; }
    }
}
